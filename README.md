# MonCompilateur - Mattia La Mura - L2 Groupe 4

**FUNCTIONALITÉS RAJOUTÉS EN PLUS DE TOUS LES TPS:**
- **FOR** = le FOR marche soit avec des DOUBLE soit avec des INTEGER. Les FOR imbriquées fonctionnent.
- **CASE** = CASE marche avec des CHAR,DOUBLE et INTEGER et VARIABLES. Dans le cas d'un char, laisser un espace entre
		 	 plusieurs valeurs d'un CaseListElement (EX: oui=|'a', 'b' : DISPLAY 1;|  non=|'a','b' : DISPLAY 1;| ).
		 	 Le dernier CaseListElement ne doit pas avoir de ';' à la fin (EX: oui=|1 : DISPLAY 1|  non=|1 : DISPLAY 1;| ).
- **SIN/COS** = Le SIN et le COS marche que avec des doubles car le resultat et dans la majorité des fois un nombre 
				avec virgule. (EX : a:=SIN(1.0); b:=COS(a);).
- **MAX/MIN** = Les fonctions MAX et MIN marchent avec des INTEGER, VARIABLES et DOUBLE. (EX : a:=MAX(1,2); b:=MIN(a,3);).
- **PROCEDURE** = PROCEDURE cree une fonction. Toutes les procedures doivent etre declarés avant le MAIN. Pour appeler une 
                  procedure ca souffit d'ecrire son nom ( EX : ....;procedure1;....).
- **SQRT** = SQRT calcule la racine carré d'un double. ( EX : a:=SQRT(64.0); ).

**Lire les commentaires de test.p pour comprendre l'execution de ./test**

A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)

**Download the repository :**

> git clone https://framagit.org/MattiaLaMura/moncompilateur.git

**Build the compiler and test it :**

> make test

**Have a look at the output :**

> gedit test.s

**Debug the executable :**

> ddd ./test

**Commit the new version :**

> git commit -a -m "What's new..."

**Send to your framagit :**

> git push -u origin master

**Get from your framagit :**

> git pull -u origin master
