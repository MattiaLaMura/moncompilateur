//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Mattia La Mura
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPES {INTEGER, BOOLEAN, CHAR, DOUBLE};



TOKEN current;				// Current token 
string *var = new string[9999]; //tableau de variables pour FOR IMBRIQUÉS
TYPES *typ = new TYPES[9999];   //et leurs type correspondant
int indice = 0; //indice tableau variable/type
string *fon = new string[9999]; //tableau de noms des fonctions
int ind = 0; //indice tableau nom fonctions


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

	
map<string, enum TYPES> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}
		
TYPES Identifier(void){
	TYPES type;
	type=DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}

TYPES Number(void){
	bool decimal=false;
	double d;
	unsigned int *i;
	string nombre=lexer->YYText();
	if(nombre.find(".")!=string::npos){
		d=atof(lexer->YYText());
		i=(unsigned int *) &d;
		cout <<"\tsubq $8,%rsp"<<endl;
		cout <<"\tmovl	$"<<*i<<", (%rsp)"<<endl;
		cout <<"\tmovl	$"<<*(i+1)<<", 4(%rsp)"<<endl;
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else{
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
}

TYPES CharConst(void){
	cout<<"\tmovq $0, %rax"<<endl;
	cout<<"\tmovb $"<<lexer->YYText()<<",%al"<<endl;
	cout<<"\tpush %rax"<<endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}


TYPES Type(void){
	if(current!=KEYWORD)
		Error("type attendu");
	if(strcmp(lexer->YYText(),"BOOLEAN")==0){
		current=(TOKEN) lexer->yylex();
		return BOOLEAN;
	}	
	else if(strcmp(lexer->YYText(),"INTEGER")==0){
		current=(TOKEN) lexer->yylex();
		return INTEGER;
	}
	else if(strcmp(lexer->YYText(),"CHAR")==0){
		current=(TOKEN) lexer->yylex();
		return CHAR;
	}
	else if(strcmp(lexer->YYText(),"DOUBLE")==0){
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else
		Error("type inconnu");
}

// VarDeclaration := Ident {"," Ident} ":" Type
void VarDeclaration(void){
	set<string> IDs;
	enum TYPES type;
	if(current!=ID)
		Error("ID attendu");
	IDs.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("ID attendu");
		IDs.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if(current!=COLON)
		Error("caractère ':' attendu");
	current=(TOKEN) lexer->yylex();
	type=Type();
	for (set<string>::iterator i=IDs.begin(); i!=IDs.end(); i++){
	    if(type==INTEGER)
	    	cout << *i << ":\t.quad 0"<<endl;
	    else if(type==DOUBLE)
	    	cout << *i << ":\t.double 0.0"<<endl;
	    else if(type==CHAR)
	    	cout << *i << ":\t.byte 0"<<endl;
	    else 
	    	Error("type inconnu");
        DeclaredVariables[*i]=type;
	}
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void){
	current=(TOKEN) lexer->yylex(); 
	VarDeclaration();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if(current!=DOT)
		Error("'.' attendu");
	current=(TOKEN) lexer->yylex();
}

TYPES Expression(void);			// Called by Term() and calls Term()

TYPES SqrtOperation(void){
	TYPES type1;
	if(!strcmp(lexer->YYText(),"SQRT")){
		current=(TOKEN) lexer->yylex();
		if(current==RPARENT){
			current=(TOKEN) lexer->yylex();
			type1=Expression();
			if(type1!=DOUBLE) Error("SQRT fonctionne que avec un double");
			cout<<"\tfldl	(%rsp)"<<endl;
			cout<<"\taddq	$8, %rsp"<<endl;
			cout<<"\tfsqrt"<<endl;
			cout<<"\tsubq $8, %rsp"<<endl;
			cout<<"\tfstpl (%rsp)"<<endl;
			if(current!=LPARENT) Error("')' attendu.");
			else {
				current=(TOKEN) lexer->yylex();
				return type1;
			}
		}
		else
			Error("'(' attendu");
	}
	else
		Error("SQRT attendu.");
}

TYPES SinOperation(void){
	TYPES type1;
	if(!strcmp(lexer->YYText(),"SIN")){
		current=(TOKEN) lexer->yylex();
		if(current==RPARENT){
			current=(TOKEN) lexer->yylex();
			type1=Expression();
			if(type1!=DOUBLE) Error("SIN fonctionne que avec un double");
			cout<<"\tpush ForRadiant"<<endl;
			cout<<"\tfldl	8(%rsp)"<<endl;
			cout<<"\tfldl	(%rsp)"<<endl;
			cout<<"\tfmulp	%st(0),%st(1)"<<endl;
			cout<<"\tfstpl 8(%rsp)"<<endl;
			cout<<"\taddq	$8, %rsp"<<endl;
			cout<<"\tpop SINorCOS"<<endl;
			cout<<"\tpush SINorCOS"<<endl;
			cout<<"\tfldl	(%rsp)"<<endl;
			cout<<"\taddq	$8, %rsp"<<endl;
			cout<<"\tfsin"<<endl;
			cout<<"\tsubq $8, %rsp"<<endl;
			cout<<"\tfstpl (%rsp)"<<endl;
			if(current!=LPARENT) Error("')' attendu.");
			else {
				current=(TOKEN) lexer->yylex();
				return type1;
			}
		}
		else
			Error("'(' attendu");
	}
	else
		Error("SIN attendu.");
}

TYPES CosOperation(void){
	TYPES type1;
	if(!strcmp(lexer->YYText(),"COS")){
		current=(TOKEN) lexer->yylex();
		if(current==RPARENT){
			current=(TOKEN) lexer->yylex();
			type1=Expression();
			if(type1!=DOUBLE) Error("COS fonctionne que avec un double");
			cout<<"\tpush ForRadiant"<<endl;
			cout<<"\tfldl	8(%rsp)"<<endl;
			cout<<"\tfldl	(%rsp)"<<endl;
			cout<<"\tfmulp	%st(0),%st(1)"<<endl;
			cout<<"\tfstpl 8(%rsp)"<<endl;
			cout<<"\taddq	$8, %rsp"<<endl;
			cout<<"\tpop SINorCOS"<<endl;
			cout<<"\tpush SINorCOS"<<endl;
			cout<<"\tfldl	(%rsp)"<<endl;
			cout<<"\taddq	$8, %rsp"<<endl;
			cout<<"\tfcos"<<endl;
			cout<<"\tsubq $8, %rsp"<<endl;
			cout<<"\tfstpl (%rsp)"<<endl;
			if(current!=LPARENT) Error("')' attendu.");
			else {
				current=(TOKEN) lexer->yylex();
				return type1;
			}
		}
		else
			Error("'(' attendu");
	}
	else
		Error("COS attendu.");
}

TYPES MaxOperation(void){
	TYPES type1, type2;
	unsigned long long tag=TagNumber++;
	if(!strcmp(lexer->YYText(),"MAX")){
		current=(TOKEN) lexer->yylex();
		if(current==RPARENT){
			current=(TOKEN) lexer->yylex();
			type1=Expression();
			if(type1==CHAR) Error("MAX() fonctionne que avec des INTEGER et des DOUBLE");
			if(current!=COMMA) Error("',' attendu.");
			cout<<"\tpop %rax"<<endl;
			current=(TOKEN) lexer->yylex();
			type2=Expression();
			if(type1!=type2) Error("Les deux elements ne sont pas du meme type");
			cout<<"\tpop %rbx"<<endl;
			cout<<"\tcmpq %rax,%rbx"<<endl;
			cout<<"\tjg Superieur"<<tag<<endl;
			cout<<"\tpush %rax"<<endl;
			cout<<"\tjmp Fin"<<tag<<endl;
			cout<<"Superieur"<<tag<<":"<<endl;
			cout<<"\tpush %rbx"<<endl;
			cout<<"Fin"<<tag<<":"<<endl;
			if(current!=LPARENT) Error("')' attendu.");
			current=(TOKEN) lexer->yylex();
			return type1;
		}
		else
			Error("'(' attendu.");
	}
	else 
		Error("MAX attendu.");
}

TYPES MinOperation(void){
	TYPES type1, type2;
	unsigned long long tag=TagNumber++;
	if(!strcmp(lexer->YYText(),"MIN")){
		current=(TOKEN) lexer->yylex();
		if(current==RPARENT){
			current=(TOKEN) lexer->yylex();
			type1=Expression();
			if(type1==CHAR) Error("MIN() fonctionne que avec des INTEGER et des DOUBLE");
			if(current==COMMA){
				cout<<"\tpop %rax"<<endl;
				current=(TOKEN) lexer->yylex();
				type2=Expression();
				if(type1!=type2) Error("Les deux elements ne sont pas du meme type");
				cout<<"\tpop %rbx"<<endl;
				cout<<"\tcmpq %rax,%rbx"<<endl;
				cout<<"\tjg Superieur"<<tag<<endl;
				cout<<"\tpush %rbx"<<endl;
				cout<<"\tjmp Fin"<<tag<<endl;
				cout<<"Superieur"<<tag<<":"<<endl;
				cout<<"\tpush %rax"<<endl;
				cout<<"Fin"<<tag<<":"<<endl;
				if(current!=LPARENT) Error("')' attendu.");
				current=(TOKEN) lexer->yylex();
				return type1;
			}
			else
				Error("',' attendu.");
		}
		else
			Error("'(' attendu.");
	}
	else 
		Error("MIN attendu.");
}

TYPES Factor(void){
	TYPES type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type = Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			type = Number();
	    else if(current==ID)
			type = Identifier();
		else if(current==CHARCONST)
			type = CharConst();
		else if(current==KEYWORD)
			if(!strcmp(lexer->YYText(),"SIN"))
				type = SinOperation();
			else if(!strcmp(lexer->YYText(),"COS"))
				type = CosOperation();
			else if(!strcmp(lexer->YYText(),"SQRT"))
				type = SqrtOperation();
			else if(!strcmp(lexer->YYText(),"MAX"))
				type = MaxOperation();
			else if(!strcmp(lexer->YYText(),"MIN"))
				type = MinOperation();
			else
				Error("'(' ou chiffre ou lettre attendue");
		else
			Error("'(' ou chiffre ou lettre attendue");
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPES Term(void){
	OPMUL mulop;
	TYPES type1, type2;
	type1 = Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2 = Factor();
		if(type1!=type2) Error("Les operandes ne sont pas du meme type");
		switch(mulop){
			case AND:
				if(type2!=BOOLEAN)
					Error("type non booléen pour l'opérateur AND");
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if(type2!=INTEGER&&type2!=DOUBLE)
					Error("type non numérique pour la multiplication");
				if(type2==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
				}
				else{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfmulp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;
			case DIV:
				if(type2!=INTEGER&&type2!=DOUBLE)
					Error("type non numérique pour la division");
				if(type2==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfdivp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;
			case MOD:
				if(type2!=INTEGER)
					Error("type non entier pour le modulo");
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPES SimpleExpression(void){
	OPADD adop;
	TYPES type1, type2;
	type1 = Term();
	while(current==ADDOP){
		adop = AdditiveOperator();		// Save operator in local variable
		type2 = Term();
		if(type1!=type2) Error("Les operandes ne sont pas du meme type");
		switch(adop){
			case OR:
				if(type2!=BOOLEAN)
					Error("opérande non booléenne pour l'opérateur OR");
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\torq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				cout << "\tpush %rax"<<endl;			// store result
				break;			
			case ADD:
				if(type2!=INTEGER&&type2!=DOUBLE)
					Error("opérande non numérique pour l'addition");
				if(type2==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax"<<endl;			// store result
				}
				else{
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;						
			case SUB:	
				if(type2!=INTEGER&&type2!=DOUBLE)
					Error("opérande non numérique pour la soustraction");
				if(type2==INTEGER){
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tsubq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax"<<endl;			// store result
				}
				else{
					cout<<"\tfldl	(%rsp)\t"<<endl;
					cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfsubp	%st(0),%st(1)\t# %st(0) <- op1 - op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl; 
				}
				break;	
			default:
				Error("opérateur additif inconnu");
		}
	}
	return type1;
}


// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPES Expression(void){
	TYPES type1, type2;
	unsigned long long tag;
	OPREL oprel;
	type1 = SimpleExpression();
	if(current==RELOP){
		tag=++TagNumber;
		oprel = RelationalOperator();
		type2 = SimpleExpression();
		if(type1!=type2) Error("Les deux operandes ne sont pas du meme type");
		if(type1!=DOUBLE){
			cout << "\tpop %rax"<<endl;
			cout << "\tpop %rbx"<<endl;
			cout << "\tcmpq %rax, %rbx"<<endl;
		}
		else{
			cout<<"\tfldl	(%rsp)\t"<<endl;
			cout<<"\tfldl	8(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
			cout<<"\t addq $16, %rsp\t# 2x pop nothing"<<endl;
			cout<<"\tfcomip %st(1)\t\t# compare op1 and op2 -> %RFLAGS and pop"<<endl;
			cout<<"\tfaddp %st(1)\t# pop nothing"<<endl;
		}
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOLEAN;
	}
	return type1;
}

void Statement(void);

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	TYPES type1, type2;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type1 = DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type2 = Expression();
	if(type1!=type2) Error("Les operandes ne sont pas du meme type");
	if(type1==CHAR){
		cout << "\tpop %rax"<<endl;
		cout << "\tmovb %al,"<<variable<<endl; 
	}
	else
		cout << "\tpop "<<variable<<endl;
	var[indice]=variable;
	typ[indice]=type1;
	indice++;
}

//<case label list> ::= <constant> {, <constant> }
TYPES CaseLabelList(unsigned long long tag){
	TYPES type1,type2;
	current=(TOKEN) lexer->yylex();
	type1=Expression();
	cout << "\tpop %rcx\t\t# mettre l'element a comparer dans rdx"<<endl;
	cout << "\tcmpq %rdx,%rcx\t\t# le comparer avec la valeur qu'on avait stocké dans rbx"<<endl;
	cout << "\tje CASEstatement"<<tag<<"\t\t# s'elles sont egales on excute l'instruction du CaseListElement "<<endl;
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		type2=Expression();
		cout << "\tpop %rcx\t\t# mettre l'element a comparer dans rdx"<<endl;
		cout << "\tcmpq %rdx,%rcx\t\t# le comparer avec la valeur qu'on avait stocké dans rbx"<<endl;
		cout << "\tje CASEstatement"<<tag<<"\t\t# s'elles sont egales on excute l'instruction du CaseListElement "<<endl;
		if(type2!=type1) Error("CaseLabelList : valeurs pas du meme type;");
	}
	cout << "\tjmp FinCASEstatement"<<tag<<"\t\t# PAS EGALES - on va au prochain CaseListElement"<<endl;
	return type1;
}
//<case list element> ::= <case label list> : <statement>
TYPES CaseListElement(unsigned long long tag,unsigned long long cas){
	TYPES type1;
	type1=CaseLabelList(tag);
	if(current==COLON){
		cout<<"CASEstatement"<<tag<<":"<<"\t\t#Debut de l'instruction de ce CaseListElement"<<endl;
		current=(TOKEN) lexer->yylex();
		Statement();
		cout<<"jmp FinCASE"<<cas<<"\t\t#si l'instruction a été executé pas besoin de voir les autres, aller à la fin du case"<<endl;
		cout<<"FinCASEstatement"<<tag<<":"<<"\t\t#Fin de ce CaseListElement, on passe au suivant"<<endl;
	}
	else
		Error("COLON attendu");
	return type1;
}
//<case statement> ::= case <expression> of <case list element> {; <case list element> } end
void CaseStatement(void){
	TYPES type1, type2;
	unsigned long long tag=TagNumber++; //tag pour les CaseListELement
	unsigned long long cas=tag;         //tag pour le CASE
	cout<<"CASE"<<tag<<":"<<endl;
	cas=tag;
	if(!strcmp(lexer->YYText(),"CASE")){
		current=(TOKEN) lexer->yylex();
		type1=Expression();
		cout << "\tpop %rdx\t\t# Prendre la variable de l'expression et la mettre dans rbx"<<endl;
		if(current!=KEYWORD||strcmp(lexer->YYText(), "OF")!=0)
			Error("OF attendue");
		type2=CaseListElement(tag,cas);
		tag++;
		if(type2!=type1) Error("CaseListElement0 pas du meme type.");
		while(current==SEMICOLON){
			type2=CaseListElement(tag,cas);
			tag++;
			if(type2!=type1) Error("CaseListElement1 pas du meme type.");		
		}
		if(current!=KEYWORD||strcmp(lexer->YYText(), "END")!=0) Error("END attendue");
		cout<<"FinCASE"<<cas<<":"<<"\t\t#fin du case"<<endl;
		current=(TOKEN) lexer->yylex();
	}
	else
		Error("mot CASE attendu.");
}


//BlockStatement := "BEGIN" Statement { ";" Statement } "END"
void BlockStatement(void){
	if(!strcmp(lexer->YYText(),"BEGIN")){
		current=(TOKEN) lexer->yylex();
		Statement();
		while(current==SEMICOLON){
			current=(TOKEN) lexer->yylex();
			Statement();
		}
		if(current!=KEYWORD||strcmp(lexer->YYText(), "END")!=0)
			Error("END attendue");
		current=(TOKEN) lexer->yylex();
	}
	else Error("BEGIN attendue");
}

// ForStatement := "FOR" AssignementStatement "To" Expression "DO" Statement
void ForStatement(void){
	TYPES type1, type2;
	unsigned long long tag=TagNumber++;
	cout<<"ForInit"<<tag<<":"<<endl;
	if(!strcmp(lexer->YYText(),"FOR")){
		current=(TOKEN) lexer->yylex();
		AssignementStatement();
		if(!strcmp(lexer->YYText(),"To")){
			cout<<"For"<<tag<<":"<<endl;
			current=(TOKEN) lexer->yylex();
			type1=Expression();
			type2=typ[indice-1];
			if(type1!=type2) Error("La varible init du for et l'expression ne sont pas du meme type");
			cout<<"\tpop %rax\t# on impile le resultat de l'expression"<<endl;
			cout<<"\tcmpq "<<var[indice-1]<<", %rax\t# on compare la varibale au resultat de l'expression"<<endl;
			cout<<"\tjnae EndFor"<<tag<<"\t#si c'est superieur ou egale on sort du for"<<endl;
			if(!strcmp(lexer->YYText(),"DO")){
				if(type2==DOUBLE){
					cout<<"\tpush "<<var[indice-1]<<endl;
					cout<<"\tpush ForDoubleIncrementation"<<endl;
					cout<<"\tfldl	8(%rsp)\t"<<endl;
					cout<<"\tfldl	(%rsp)\t# first operand -> %st(0) ; second operand -> %st(1)"<<endl;
					cout<<"\tfaddp	%st(0),%st(1)\t# %st(0) <- op1 + op2 ; %st(1)=null"<<endl;
					cout<<"\tfstpl 8(%rsp)"<<endl;
					cout<<"\taddq	$8, %rsp\t# result on stack's top"<<endl;
					cout<<"\tpop "<<var[indice-1]<<endl;
				}
				else cout<<"\taddq $1,"<<var[indice-1]<<endl;
				indice--;
				current=(TOKEN) lexer->yylex();
				Statement();
				cout<<"\tjmp For"<<tag<<endl;
				cout<<"EndFor"<<tag<<":"<<endl;
			}
			else Error("DO attendue");
		}
		else Error("To attendue");
	}
	else Error("FOR attendue");
}

// WhileStatement := "WHILE" Expression DO Statement
void WhileStatement(void){
	unsigned long long tag=TagNumber++;
	cout<<"While"<<tag<<":"<<endl;
	if(!strcmp(lexer->YYText(),"WHILE")){
		current=(TOKEN) lexer->yylex();
		if(Expression()!=BOOLEAN) Error("L'expression n'est pas de type BOOLEAN");
		cout<<"\tpop %rax\t# on impile le resultat de l'expression"<<endl;
		cout<<"\tcmpq $0, %rax\t# on compare 0 et le resultat"<<endl;
		cout<<"\tje EndWhile"<<tag<<"\t# si c'est zero on sort du WHILE"<<endl;
		if(current!=KEYWORD||strcmp(lexer->YYText(), "DO")!=0)
			Error("mot-clé DO attendu");
		current=(TOKEN) lexer->yylex();
		Statement();
		cout<<"\tjmp While"<<tag<<endl;
		cout<<"EndWhile"<<tag<<":"<<endl;
	}
	else Error("WHILE attendue");
}

// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]
void IfStatement(void){
	TYPES type1;
	unsigned long long tag=TagNumber++;
	if(!strcmp(lexer->YYText(),"IF")){
		current=(TOKEN) lexer->yylex();
		type1=Expression();
		if(type1!=BOOLEAN) Error("L'expression n'est pas de type BOOLEAN");
		cout<<"\tpop %rax\t# on impile le resultat de l'expression"<<endl;
		cout<<"\tcmpq $0, %rax\t# on compare 0 et le resultat"<<endl;
		cout<<"\tje Else"<<tag<<"\t# si c'est zero on continue sinon on passe au ELSE"<<endl;
		if(!strcmp(lexer->YYText(),"THEN")){
			current=(TOKEN) lexer->yylex();
			Statement();
			cout<<"\tjmp Next"<<tag<<"\t# if termine on passe à la suite"<<endl;
			cout<<"\tElse"<<tag<<":"<<endl;
			if(!strcmp(lexer->YYText(),"ELSE")){
				current=(TOKEN) lexer->yylex();
				Statement();
			}
			cout<<"Next"<<tag<<":"<<endl;
		}
		else Error("THEN attendue");
	}
	else Error("IF attendue");
}

// Display := "DISPLAY" Expression
void Display(void){
	TYPES type;
	unsigned long long tag=++TagNumber;
	current=(TOKEN) lexer->yylex();
	type=Expression();
	if(type==INTEGER){
		cout << "\tpop %rsi\t# The value to be displayed"<<endl;
		cout << "\tmovq $FormatString1, %rdi\t# \"%llu\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;
	}
	else if(type==BOOLEAN){
		cout << "\tpop %rdx\t# Zero : False, non-zero : true"<<endl;
		cout << "\tcmpq $0, %rdx"<<endl;
		cout << "\tje False"<<tag<<endl;
		cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\""<<endl;
		cout << "\tjmp Next"<<tag<<endl;
		cout << "False"<<tag<<":"<<endl;
		cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\""<<endl;
		cout << "Next"<<tag<<":"<<endl;
		cout << "\tcall	puts@PLT"<<endl;
	}
	else if(type==DOUBLE){
		cout << "\tmovsd	(%rsp), %xmm0\t\t# &stack top -> %xmm0"<<endl;
		cout << "\tsubq	$16, %rsp\t\t# allocation for 3 additional doubles"<<endl;
		cout << "\tmovsd %xmm0, 8(%rsp)"<<endl;
		cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\""<<endl;
		cout << "\tmovq	$1, %rax"<<endl;
		cout << "\tcall	printf"<<endl;
		cout << "nop"<<endl;
		cout << "\taddq $24, %rsp\t\t\t# pop nothing"<<endl;
	}
	else if(type==CHAR){
		cout<<"\tpop %rsi\t\t\t# get character in the 8 lowest bits of %si"<<endl;
		cout << "\tmovq $FormatString3, %rdi\t# \"%c\\n\""<<endl;
		cout << "\tmovl	$0, %eax"<<endl;
		cout << "\tcall	printf@PLT"<<endl;
	}
	else
		Error("DISPLAY non disponible pour ce type.");
}

// ProcedureDeclaration := <NomFonction> ";" BlockStatement
void ProcedureDeclaration(void){
	string nom;
	current=(TOKEN) lexer->yylex();
	if(current!=ID) Error("ID attendu.");
	nom=lexer->YYText();
	fon[ind]=nom;
	cout<<fon[ind]<<":"<<endl;
	ind++;
	current=(TOKEN) lexer->yylex();
	if(current!=SEMICOLON) Error("';' attendu.");
	current=(TOKEN) lexer->yylex();
	BlockStatement();
	cout<<"\tret"<<endl;
}

void ProcedureCall(int i){
	cout<<"\tcall "<<fon[i]<<endl;
	current=(TOKEN) lexer->yylex();
}

// AssignementStatement | IfStatement | WhileStatement | ForStatement | BlockStatement | Display | Case | Procedure
void Statement(void){
	if(current==ID){
		bool assignement=true;
		for(int i=0;i<ind;i++){
			if(fon[i]==lexer->YYText()){
				assignement=false;
				ProcedureCall(i);
				break;
			}
		}
		if(assignement==true) AssignementStatement();
	} 
	else if(current==KEYWORD){
		if(!strcmp(lexer->YYText(),"WHILE")){
			WhileStatement();
		}
		else if(!strcmp(lexer->YYText(),"IF")){
			IfStatement();
		}
		else if(!strcmp(lexer->YYText(),"FOR")){
			ForStatement();
		}
		else if(!strcmp(lexer->YYText(),"BEGIN")){
			BlockStatement();
		}
		else if(!strcmp(lexer->YYText(),"DISPLAY")){
			Display();
		}
		else if(!strcmp(lexer->YYText(),"CASE")){
			CaseStatement();
		}
		else if(!strcmp(lexer->YYText(),"PROCEDURE")){
			Error("PROCEDURE doit etre avant le main");
		}
	}
	else Error("ID ou KEYWORD attendue");
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.align 8"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [VarDeclarationPart] StatementPart
void Program(void){
	if(current==KEYWORD && strcmp(lexer->YYText(), "VAR")==0)
		VarDeclarationPart();
	while((!strcmp(lexer->YYText(),"PROCEDURE"))){
		ProcedureDeclaration();
		current=(TOKEN) lexer->yylex();
	}
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the Mattia La Mura"<<endl;
	cout << ".data"<<endl;
	cout << "FormatString1:\t.string \"%llu\"\t# used by printf to display 64-bit unsigned integers"<<endl; 
	cout << "FormatString2:\t.string \"%lf\"\t# used by printf to display 64-bit floating point numbers"<<endl; 
	cout << "FormatString3:\t.string \"%c\"\t# used by printf to display a 8-bit single character"<<endl; 
	cout << "TrueString:\t.string \"TRUE\"\t# used by printf to display the boolean value TRUE"<<endl; 
	cout << "FalseString:\t.string \"FALSE\"\t# used by printf to display the boolean value FALSE"<<endl; 
	cout << "ForDoubleIncrementation: .double 1.0"<<endl;
	cout << "SINorCOS: .double 0.0"<<endl;
	cout << "ForRadiant: .double 0.01745392\t\t#=(pigreque/180)"<<endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}